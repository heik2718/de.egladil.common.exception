//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.common.exception;

/**
 * NotImplementedException
 */
public class NotImplementedException extends RuntimeException {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	/**
	 * Erzeugt eine Instanz von NotImplementedException
	 */
	public NotImplementedException(String message) {
		super(message);
	}

	/**
	 * Erzeugt eine Instanz von NotImplementedException
	 */
	public NotImplementedException(Throwable cause) {
		super(cause);
	}

	/**
	 * Erzeugt eine Instanz von NotImplementedException
	 */
	public NotImplementedException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Erzeugt eine Instanz von NotImplementedException
	 */
	public NotImplementedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
