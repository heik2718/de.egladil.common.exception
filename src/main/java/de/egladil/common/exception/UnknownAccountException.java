//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.common.exception;

/**
 * UnknownAccountException
 */
public class UnknownAccountException extends EgladilAuthenticationException {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	/**
	 * Erzeugt eine Instanz von UnknownAccountException
	 */
	public UnknownAccountException(String message) {
		super(message);
	}

	/**
	 * Erzeugt eine Instanz von UnknownAccountException
	 */
	public UnknownAccountException(Throwable cause) {
		super(cause);
	}

	/**
	 * Erzeugt eine Instanz von UnknownAccountException
	 */
	public UnknownAccountException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Erzeugt eine Instanz von UnknownAccountException
	 */
	public UnknownAccountException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
