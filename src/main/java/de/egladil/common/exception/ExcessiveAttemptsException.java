//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.common.exception;

/**
* ExcessiveAttemptsException
*/
public class ExcessiveAttemptsException extends EgladilAuthenticationException {

	/* serialVersionUID		*/
	private static final long serialVersionUID = 1L;

	/**
	* Erzeugt eine Instanz von ExcessiveAttemptsException
	*/
	public ExcessiveAttemptsException(String message) {

		super(message);
	}

	/**
	* Erzeugt eine Instanz von ExcessiveAttemptsException
	*/
	public ExcessiveAttemptsException(Throwable cause) {

		super(cause);
	}

	/**
	* Erzeugt eine Instanz von ExcessiveAttemptsException
	*/
	public ExcessiveAttemptsException(String message, Throwable cause) {

		super(message, cause);
	}

	/**
	* Erzeugt eine Instanz von ExcessiveAttemptsException
	*/
	public ExcessiveAttemptsException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {

		super(message, cause, enableSuppression, writableStackTrace);
	}

}
