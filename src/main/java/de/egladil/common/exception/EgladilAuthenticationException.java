//=====================================================
// Projekt: de.egladil.exceptions
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.common.exception;

/**
 * @author heike
 *
 */
public class EgladilAuthenticationException extends RuntimeException {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * @param message
	 */
	public EgladilAuthenticationException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public EgladilAuthenticationException(Throwable cause) {
		super(cause);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public EgladilAuthenticationException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public EgladilAuthenticationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
