//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.common.exception;

/**
 * DisabledAccountException
 */
public class DisabledAccountException extends RuntimeException {

	/* serialVersionUID */
	private static final long serialVersionUID = 2L;

	/**
	 * Erzeugt eine Instanz von DisabledAccountException
	 */
	public DisabledAccountException(String message) {
		super(message);
	}

	/**
	 * Erzeugt eine Instanz von DisabledAccountException
	 */
	public DisabledAccountException(Throwable cause) {
		super(cause);
	}

	/**
	 * Erzeugt eine Instanz von DisabledAccountException
	 */
	public DisabledAccountException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Erzeugt eine Instanz von DisabledAccountException
	 */
	public DisabledAccountException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
