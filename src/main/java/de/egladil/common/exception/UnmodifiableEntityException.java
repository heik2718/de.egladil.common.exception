//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.common.exception;

/**
 * UnmodifiableEntityException
 */
public class UnmodifiableEntityException extends RuntimeException {

	private static final String DEFAULT_MESSAGE = "Die Entity kann nicht mehr geändert werden (eingefroren)";

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	/**
	 * Erzeugt eine Instanz von UnmodifiableEntityException
	 */
	public UnmodifiableEntityException() {
		super(DEFAULT_MESSAGE);
	}

	/**
	 * Erzeugt eine Instanz von UnmodifiableEntityException
	 */
	public UnmodifiableEntityException(String message) {
		super(message);
	}

	/**
	 * Erzeugt eine Instanz von UnmodifiableEntityException
	 */
	public UnmodifiableEntityException(Throwable cause) {
		super(DEFAULT_MESSAGE, cause);
	}

	/**
	 * Erzeugt eine Instanz von UnmodifiableEntityException
	 */
	public UnmodifiableEntityException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Erzeugt eine Instanz von UnmodifiableEntityException
	 */
	public UnmodifiableEntityException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
