//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.common.exception;

/**
 * ResourceNotFountException
 */
public class ResourceNotFoundException extends RuntimeException {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	/**
	 * Erzeugt eine Instanz von ResourceNotFountException
	 */
	public ResourceNotFoundException(String message) {
		super(message);
	}

	/**
	 * Erzeugt eine Instanz von ResourceNotFountException
	 */
	public ResourceNotFoundException(Throwable cause) {
		super(cause);
	}

	/**
	 * Erzeugt eine Instanz von ResourceNotFountException
	 */
	public ResourceNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Erzeugt eine Instanz von ResourceNotFountException
	 */
	public ResourceNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
