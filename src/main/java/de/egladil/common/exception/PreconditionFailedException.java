//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.common.exception;

/**
 * PreconditionFailedException wird verwendet, wenn eine Anwenderaktion nicht erlaubt ist, weil fachliche Gründe dagegen sprechen.
 */
public class PreconditionFailedException extends RuntimeException {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	/**
	 * PreconditionFailedException
	 */
	public PreconditionFailedException(final String message) {
		super(message);
	}

	/**
	 * PreconditionFailedException
	 */
	public PreconditionFailedException(final Throwable cause) {
		super(cause);
	}

	/**
	 * PreconditionFailedException
	 */
	public PreconditionFailedException(final String message, final Throwable cause) {
		super(message, cause);
	}

	/**
	 * PreconditionFailedException
	 */
	public PreconditionFailedException(final String message, final Throwable cause, final boolean enableSuppression,
		final boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
