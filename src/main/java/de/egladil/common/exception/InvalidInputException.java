//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.common.exception;

/**
* InvalidInputException
*/
public class InvalidInputException extends RuntimeException {

	/* serialVersionUID		*/
	private static final long serialVersionUID = 1L;

/**
	* InvalidInputException
	*/
	public InvalidInputException(final String arg0) {
		super(arg0);
	}

	/**
	* InvalidInputException
	*/
	public InvalidInputException(final String arg0, final Throwable arg1) {
		super(arg0, arg1);
	}

}
