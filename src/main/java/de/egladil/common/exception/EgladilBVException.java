//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.common.exception;

/**
* EgladilBVException
*/
public class EgladilBVException extends RuntimeException {

	/* serialVersionUID		*/
	private static final long serialVersionUID = 1L;


	/**
	* Erzeugt eine Instanz von EgladilBVException
	*/
	public EgladilBVException(String message) {
		super(message);
	}

	/**
	* Erzeugt eine Instanz von EgladilBVException
	*/
	public EgladilBVException(Throwable cause) {
		super(cause);
	}

	/**
	* Erzeugt eine Instanz von EgladilBVException
	*/
	public EgladilBVException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	* Erzeugt eine Instanz von EgladilBVException
	*/
	public EgladilBVException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
